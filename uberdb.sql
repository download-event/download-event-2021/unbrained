-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Set 19, 2021 alle 16:32
-- Versione del server: 10.4.21-MariaDB
-- Versione PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uberdb`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `travels`
--

CREATE TABLE `travels` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `place_from` varchar(50) NOT NULL DEFAULT '0',
  `place_to` varchar(50) NOT NULL DEFAULT '0',
  `arrival_time` time NOT NULL DEFAULT '00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `travels`
--

INSERT INTO `travels` (`id`, `user_id`, `place_from`, `place_to`, `arrival_time`) VALUES
(1, 2, 'milano', 'bergamo', '17:49:00'),
(2, 2, 'pontida', 'bergamo', '17:49:00'),
(3, 2, 'prova', 'bergamo', '17:27:00'),
(4, 2, 'pontida', 'milano', '18:26:00'),
(5, 5, 'pontida', 'milano', '18:31:00');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `codice_greenpass` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`user_id`, `username`, `email`, `password`, `codice_greenpass`) VALUES
(1, 'prova', 'prova@prova.com', '$2y$10$egPtmZJxp9d7qVttwhM/UuAiqljba282C.E0447vHX6nK80pmKLwS', 'provaprova'),
(2, 'vamo', 'vamo@vamo.com', '$2y$10$/LXBqvBF7z2xlY5ZRVP0H.Ykm6W/zRrwnLVlGWO4PNUVGLLy5RY.i', 'vamo'),
(3, 'email', 'email@email.com', '$2y$10$.15Zu4CNqArIDxOEAJn4sORflk6/54iD4yPudLAL7/6AQkdZ.YsBK', '1231231231231231233123123'),
(4, 'gabbomazzo', 'pinco@pallino.com', '$2y$10$jND6TGWVEic7BNXmHxn1FOGtfe8zNbaTD0ZPN31MQCCtrrXTYnfM2', '123456'),
(5, 'prova2', 'prova2@prova.com', '$2y$10$Sk6nemG.kmR3djM4GHiM2ektulnEVR6iFOepyzlvAbIDjT6y.ETDC', '1234124');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `travels`
--
ALTER TABLE `travels`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `travels`
--
ALTER TABLE `travels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
